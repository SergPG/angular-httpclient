import React from 'react';
//import Brand from './Brand';
//import { Button } from 'reactstrap';

import { Route, Redirect } from "react-router-dom";

import Dashboard from '../Dashboard/Dashboard';
import Tables from '../Tables/Tables';


function ComponentButtons() {
  return (
    <div className="Tables container-fluid">
       
      <h1 className="h3 mb-4 text-gray-800">Custom Components: Buttons </h1>

    </div>
  );

}

function ComponentCards() {
  return (
    <div className="Tables container-fluid">
       
      <h1 className="h3 mb-4 text-gray-800">Custom Components: Cards </h1>

    </div>
  );
}


class Content extends React.Component {

  
 
  render() {
  return (

    <div id="content">

  {/*  <Route exact path="/" render={() => (
  		loggedIn ? ( <Redirect to="/dashboard"/> ) : (
   					 <PublicHomePage/>  )
		)}/>*/}
    
     <Route path="/" render={() => (
  		 <Redirect to="/dashboard"/> 
		)}/>


     <Route exact  path="/dashboard" component={Dashboard} />

     <Route path="/tables" component={Tables} />

      <Route path="/components/buttons" component={ComponentButtons} />
      <Route path="/components/cards" component={ComponentCards} />
   

     {/*<Button color="danger">Danger!</Button>*/}

    </div>
   
 	 );
	}
}

export default Content;
