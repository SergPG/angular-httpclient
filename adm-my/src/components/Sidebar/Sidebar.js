import React from 'react';
import Brand from './Brand';

import { NavLink } from "react-router-dom";

import {  
       UncontrolledDropdown,
       DropdownToggle, 
       DropdownMenu,
       DropdownItem } from 'reactstrap';

class Sidebar extends React.Component {

  constructor(props) {
    super(props);

    this.state = { dropdownOpen: false };
   
    this.handleClick = this.handleClick.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  handleClick(event) {
    this.props.onClickToggle(event);
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }


  render() {
  return (

    

    <ul  className={  this.props.isToggleOn ? 'navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled' : 'navbar-nav bg-gradient-primary sidebar sidebar-dark accordion' } id="accordionSidebar">

       <Brand title={ "SB Admin" } />


    
    <hr className="sidebar-divider my-0" />

    <li className="nav-item">
        <NavLink exact  to="/dashboard" className="nav-link ">
           <i className="fas fa-fw fa-tachometer-alt"></i>
              <span>Dashboard</span>
        </NavLink>    
    </li>


      <hr className="sidebar-divider" />

     {/* <!-- Heading -->*/}
      <div className="sidebar-heading">
        Interface
      </div>


        {/* <!-- Nav Item - Tables -->*/}

      <li className="nav-item">
        <NavLink to="/tables" className="nav-link">
         <i className="fas fa-fw fa-table"></i>
            <span>Tables</span>
        </NavLink> 
      </li>

      <UncontrolledDropdown nav inNavbar>
          <DropdownToggle tag="a" 
                          className={ this.props.isResize ? 'nav-link collapsed isResize' : this.props.isToggleOn  ? 'nav-link' : 'nav-link collapsed'}  
                          aria-controls="collapseTwo"
                          data-toggle={  this.props.isToggleOn  ? 'collapse' : ''}
                          data-target="#collapseTwo"
                          
                          caret>
            <i className="fas fa-fw fa-cog"></i>
            <span>Components</span>
          </DropdownToggle>
          <DropdownMenu 
            id="collapseTwo" 
            className={'collapse'} 
            aria-labelledby="headingTwo" 
            data-parent="#accordionSidebar">

            <div className="bg-white py-2 collapse-inner rounded">
              <DropdownItem header>Custom Components:</DropdownItem>
                <NavLink to="/components/buttons" className="collapse-item" >
                    Buttons
                </NavLink> 
                <NavLink to="/components/cards" className="collapse-item" >
                    Cards
                </NavLink> 
            </div>  
          </DropdownMenu>
      </UncontrolledDropdown>

     {/* <!-- Divider -->*/}
      <hr className="sidebar-divider d-none d-md-block" />

     {/* <!-- Sidebar Toggler (Sidebar) -->*/}
      <div className="text-center d-none d-md-inline">
        <button className="rounded-circle border-0"
                id="sidebarToggle"
                onClick={this.handleClick}></button>
      </div>

    </ul>
   
   );
  }
}

export default Sidebar;
