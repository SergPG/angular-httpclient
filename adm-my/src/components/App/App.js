import React from 'react';
// import './App.scss';

import Sidebar from '../Sidebar/Sidebar';
import Topbar from '../Topbar/Topbar';
import Content from '../Content/Content';
import Footer from '../Footer/Footer';

import { BrowserRouter as Router } from "react-router-dom";

class App extends React.Component {

	constructor(props) {
    super(props);
    
    this.state = {
    	isScrollOn: false,
    	isToggleOn: false,
    	isResize: false };

    this.scrollStart = 100;
    } 

	componentDidMount() {
	    window.addEventListener('scroll', this.handleScroll);
	    window.addEventListener('resize', this.handleResize);
	    this.handleResize();   
	}


    handleResize = (event)=>{
    	if(Math.round(window.innerWidth) < 780) {
    		this.setState(stateResize => ({
	        isResize:  true
	        }));
	    } else {
           this.setState(stateResize => ({
	        isResize: false
	        }));
		}
    	
    	console.log('handleResize', this.state.isResize );
    }


    handleClickToggleOn = (event)=>{
    	this.setState(state => ({
          isToggleOn: !state.isToggleOn }
          ));
    	// debugger;
        console.log('По ссылке кликнули.', this.state.isToggleOn);
    } 	


	handleClickToTop = (event)=>{
	    event.preventDefault();
	    window.scrollTo(0, 0);
	    console.log('По ссылке scroll-to-top кликнули.');
	    console.log('isScrollOn', this.state.isScrollOn );
	}
    
    handleScroll = (event)=>{
      let scrollDistance = Math.round(window.scrollY);
      	if(scrollDistance > this.scrollStart) {
	    	this.setState(stateScroll => ({
	        isScrollOn:  true
	        }));
	    } else {
           this.setState(stateScroll => ({
	        isScrollOn: false
	        }));
		}	
    }

 render() {
    const isResize = this.state.isResize;
    const isToggleOn = this.state.isToggleOn;
    
    console.log('isToggleOn', isToggleOn );
    console.log('isResize', isResize );

  return (

	   <Router>
	   <div className={isToggleOn ? 'sidebar-toggled' : ''} >
			 <div id="wrapper" >   
			       <Sidebar
			        isResize={isResize} 
			        isToggleOn={isToggleOn} 
                    onClickToggle={this.handleClickToggleOn}  />

			    <div id="content-wrapper" className="d-flex flex-column">   
		           <Topbar onClickToggle={this.handleClickToggleOn}  />
			       <Content />
			       <Footer />
			    </div>   
			 </div>

		  <a className={this.state.isScrollOn ? 'scroll-to-top rounded visible' : 'scroll-to-top rounded invisible' }  
		     onClick={ this.handleClickToTop }
		     style={{display:'inline'}}  
		     href="#page-top">
		    <i className="fas fa-angle-up"></i>
		  </a>
	  </div>

	   </Router> 
  );
 }
}

export default App;
