import React from 'react';
import stl from './Header.module.css';


function Header() {
  return (
      <header id="pageHeader" className={stl.pageHeader}>
        Header
      </header>  
  );
}

export default Header;
