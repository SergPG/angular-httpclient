import React from 'react';
import stl from './MainNav.module.css';

function MainNav(props) {
  return (
      <nav id="mainNav" className={stl.mainNav}>
          <div className={stl.item}>
          	 <a className={`${stl.link} ${stl.active}`} href="/" >
		          Item 1
		      </a>	
          </div>
          <div className={stl.item}>
          	 <a className={stl.link} href="/" >
		          Item 2
		      </a>	
          </div>
          <div className={stl.item}>
          	 <a className={stl.link} href="/" >
		          Item 3
		      </a>	
          </div>
       </nav>
  );
}

export default MainNav;
