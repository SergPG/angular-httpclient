import React from 'react';
import stl from './Article.module.css';

function Item (props){
  return (
      <div className={stl.item} >
           <h3 className={stl.title}> {props.post.title} </h3>
             <p> {props.post.content} </p>   
         </div>
    );
};

function Article(props) {

    const posts = props.posts;
    
    const listPosts = posts.map((post)=>
      <Item key={post.id} post={post} /> );

  return ( 
     <article id="mainArticle"  className={stl.mainArticle}>
       
       <div className={stl.articles} > 
         <h2 className={stl.header}> Статьи  </h2>
         
         { listPosts }
          
       </div>
     </article>  
  );
}

export default Article;
