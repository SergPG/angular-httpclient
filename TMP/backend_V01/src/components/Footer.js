import React from 'react';
import stl from './Footer.module.css';


function Copyright(){
  return(
       <section className={stl.copyright}>
          
            <small>Copyright &copy; Your Website 2019</small>
        
        </section> 
    );
}

function Footer() {
  return (
          <footer id="pageFooter" className={stl.pageFooter}>
            <Copyright />
          </footer>
  );
}

export default Footer;
