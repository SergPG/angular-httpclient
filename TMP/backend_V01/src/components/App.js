import React from 'react';
import './App.css';

import Header from './Header';
import MainNav from './MainNav';
import Article from './Article';
import Footer from './Footer';


const posts = [
  {id: 1, title: 'Привет, мир', content: 'Добро пожаловать в документацию React!'},
  {id: 2, title: 'Установка', content: 'React можно установить из npm.'},
  {id: 3, title: 'Элементы', content: 'Элементы — мельчайшие кирпичики React-приложений.'}
];

function App() {
  return (
    <div id="appMain" className="App">
      <Header />
      <MainNav />
      <Article posts={posts} />
      <Footer />
    </div>
  );
}

export default App;
