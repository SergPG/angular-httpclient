import React from 'react';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';


function NavAnimate(props) {

  // const handleSelect = (selectedKey, event)=> {
  //             if(props.isStyleOn){
  //              // var selectLink = event.target.classList.contains("active");

  //               var selectLink = event.target.classList;

  //              console.log('selectLink',selectLink); 
  //             //alert(`selected ${selectedKey}`)
  //             }
  //           }


   return (

    <ul className="navbar-nav ml-auto">
      <li className="nav-item mx-0 mx-lg-1">
        <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio" onClick={props.handleSelect}>Portfolio</a>
      </li>
      <li className="nav-item mx-0 mx-lg-1">
        <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">About</a>
      </li>
      <li className="nav-item mx-0 mx-lg-1">
        <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Contact</a>
      </li>
    </ul>


    );

}




//============= Page Section Navigation  ======================
class Navigation extends React.Component {

   constructor(props) {
       super(props);
       
       this.handleSelect = this.handleSelect.bind(this);   
  }


   handleSelect(event){
              // if(this.props.isStyleOn){
               // var selectLink = event.target.classList.contains("active");

                var selectLink = event.target.classList.toggle("active");

               console.log('selectLink',selectLink); 
              //alert(`selected ${selectedKey}`)
             // }
            }

   


  render() {
    return (   

//======================================

<Navbar id="mainNav" bg="secondary" expand="lg" fixed="top" className={ this.props.isStyleOn ? 'text-uppercase navbar-shrink' : 'text-uppercase' }>
 <div className="container">

  <Navbar.Brand href="#page-top">Start React</Navbar.Brand>

  <Navbar.Toggle aria-controls="navbarResponsive" 
    className="navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" 
    children={<span>Menu <i className="fas fa-bars"></i></span>} 
    />

  <Navbar.Collapse id="navbarResponsive">

   {/* <Nav className="ml-auto" as="ul">
     <Nav.Item as="li" className="mx-0 mx-lg-1">
        <Nav.Link className="py-3 px-0 px-lg-3 rounded js-scroll-trigger"  href="#portfolio">Portfolio</Nav.Link>
     </Nav.Item>
     <Nav.Item as="li" className="mx-0 mx-lg-1"> 
        <Nav.Link className="py-3 px-0 px-lg-3 rounded js-scroll-trigger"  href="#about">About</Nav.Link>
     </Nav.Item> 
     <Nav.Item as="li" className="mx-0 mx-lg-1"> 
        <Nav.Link className="py-3 px-0 px-lg-3 rounded js-scroll-trigger"  href="#contact">Contact</Nav.Link>
     </Nav.Item> 
    </Nav>*/}

    <NavAnimate isStyleOn={this.props.isStyleOn} handleSelect={this.handleSelect}/>


  </Navbar.Collapse>

 </div> 
</Navbar>

//====================================


     
  // <nav className="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
  //   <div className="container">
  //     <a className="navbar-brand js-scroll-trigger" href="#page-top">Start Bootstrap</a>
  //     <button className="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
  //       Menu
  //       <i class="fas fa-bars"></i>
  //     </button>
  //     <div className="collapse navbar-collapse" id="navbarResponsive">

  //       <ul className="navbar-nav ml-auto">
  //         <li className="nav-item mx-0 mx-lg-1">
  //           <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Portfolio</a>
  //         </li>
  //         <li className="nav-item mx-0 mx-lg-1">
  //           <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">About</a>
  //         </li>
  //         <li className="nav-item mx-0 mx-lg-1">
  //           <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Contact</a>
  //         </li>
  //       </ul>

  //     </div>
  //   </div>
  // </nav>
   
     );
  } 
}

export default Navigation;