import { Injectable } from '@angular/core';

import { HttpClient, HttpParams,
         HttpRequest, HttpHeaders,
         HttpErrorResponse } from  "@angular/common/http";

import { Observable, throwError } from  "rxjs";
import { map, tap, catchError, retry } from  "rxjs/operators";




export interface Customer {

id : number;
name: string;
email: string;
phone: string;

}

//const  params = new  HttpParams({fromString:  '_page=1&_limit=15'});

@Injectable({
  providedIn: 'root'
})
export class HttpService {


  usersUrl = 'http://localhost:3000/projects';


  constructor(
     private http: HttpClient
    // private httpHeaders: HttpHeaders
  	) { }


  getAll():Observable<Customer[]>{ 	
  	return this.http.get<Customer[]>(this.usersUrl, {observe: 'response'})
      .pipe(
          tap(response => console.log(response)),
          map(response => response.body),
        );
  }  // End getAll
//-------------------------------------------

  getFanErr():Observable<Customer[]>{    
    return this.http.get<Customer[]>(this.usersUrl)
      .pipe(
          catchError(this.hendleError),
        );
  }  // End getFanErr
     
  hendleError(error : HttpErrorResponse){
     console.log(error);
     return throwError(error);  
  }
//------------------------------------


  getFanErrRetry():Observable<Customer[]>{    
    return this.http.get<Customer[]>(this.usersUrl)
      .pipe(
          retry(3),
          catchError(this.hendleErrorRetry), 
        );
  }  // End getFanErrRetry
     
  hendleErrorRetry(error : HttpErrorResponse){
     console.log('error occured serv', error);
     return throwError(error);  
  }
//------------------------------------


  getFanErrRetryHeaders():Observable<Customer[]>{   
    const headers = new HttpHeaders({
      'Name': 'Serg'
    });

    let params = new HttpParams().append('_page','2');
        params = params.append('_limit','6');

    return this.http.get<Customer[]>(this.usersUrl, {params: params, headers: headers})
      .pipe(
          retry(3),
          catchError(this.hendleErrorRetryHeaders), 
        );
  }  // End getFanErrRetryHeaders
     
  hendleErrorRetryHeaders(error : HttpErrorResponse){
     console.log('error occured serv', error);
     return throwError(error);  
  }
//------------------------------------


  getFanInterceptorHeaders():Observable<Customer[]>{   
    // const headers = new HttpHeaders({
    //   'Name': 'Serg'
    // });

    // let params = new HttpParams().append('_page','2');
    //     params = params.append('_limit','6');

    return this.http.get<Customer[]>(this.usersUrl);
      
  }  // End getFanInterceptorHeaders
     
  // hendleInterceptorHeaders(error : HttpErrorResponse){
  //    console.log('error occured serv', error);
  //    return throwError(error);  
  // }
//------------------------------------


}
