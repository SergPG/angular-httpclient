import { Injectable } from '@angular/core';

import { HttpInterceptor, HttpRequest, HttpHandler,
         HttpEvent, HttpHeaders, HttpErrorResponse } from  "@angular/common/http";

import { Observable, throwError } from  "rxjs";
import { map, tap, catchError, retry } from  "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{

  constructor() { }

  // hendleError(error : HttpErrorResponse){
  //    console.log('Error !!',error);
  //    return throwError(error);  
  // }

  // intercept(req: HttpRequest<any>, next: HttpHandler):
  // Observable<HttpEvent<any>>{
  //     const headers = new HttpHeaders({
  //     	'Authorization': 'sergpopov'
  //     });

  //     const clone = req.clone({
  //     	headers: headers
  //     });

  // 	return next.handle(clone)
  // 	.pipe(
  //         catchError(this.hendleError),
  //       );
  // };

 hendleError(error : HttpErrorResponse){
     console.log('Error !!',error);
     return throwError(error);  
  }

  intercept(req: HttpRequest<any>, next: HttpHandler):
  Observable<HttpEvent<any>>{
    
      const headers = new HttpHeaders({
        'Authorization': 'sergpopov'
      });

      const clone = req.clone({
        headers: headers
      });

    return next.handle(clone)
    .pipe(
          catchError(this.hendleError),
        );
  };


}
