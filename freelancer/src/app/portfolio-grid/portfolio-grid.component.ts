import { Component, OnInit, Input } from '@angular/core';
import { HttpService, Portfolio } from '../http.service';


@Component({
  selector: 'app-portfolio-grid',
  templateUrl: './portfolio-grid.component.html',
  styleUrls: ['./portfolio-grid.component.scss']
})
export class PortfolioGridComponent implements OnInit {

  portfolios;

  constructor(
    private httpService: HttpService
  	) { }

  ngOnInit() {

  	this.httpService.getPortfolios().subscribe(data => {
  		 this.portfolios = data;
     	//console.log(data[0].title)
     	console.log('Pportfolios ',this.portfolios)
     });
  }

}
