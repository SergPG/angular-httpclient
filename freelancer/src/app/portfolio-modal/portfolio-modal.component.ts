import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

import {NgbActiveModal, NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './portfolio-modal-content.html',
  // template: `
  //   <div class="modal-header">
  //     <h4 class="modal-title">Hi there!</h4>
  //     <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
  //       <span aria-hidden="true">&times;</span>
  //     </button>
  //   </div>
  //   <div class="modal-body">
  //     <p>Hello, {{name}}!</p>
  //   </div>
  //   <div class="modal-footer">
  //     <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
  //   </div>
  // `
})
export class PortfolioModalContent {
  @Input() portfolio;

  constructor(public activeModal: NgbActiveModal) {}
}


@Component({
  selector: 'app-portfolio-modal',
  templateUrl: './portfolio-modal.component.html',
  styleUrls: ['./portfolio-modal.component.scss']
})
export class PortfolioModalComponent implements OnInit {

 @Input() portfolio;

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }
  
  open(){
  	 const modalRef = this.modalService.open(PortfolioModalContent,{ size: 'xl' });
  	 modalRef.componentInstance.portfolio = this.portfolio;
     console.log('Image',this.portfolio.image); 
  } 

}
