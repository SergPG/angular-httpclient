import { Injectable } from '@angular/core';

import { HttpClient, HttpParams,
         HttpRequest, HttpHeaders,
         HttpErrorResponse } from  "@angular/common/http";

import { Observable, throwError } from  "rxjs";
import { map, tap, catchError, retry } from  "rxjs/operators";

export interface Portfolio {
		id : number;
		title: string;
		description: string;
		image: string;
		}

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  portfolioUrl = 'http://localhost:3000/projects';	

  constructor( private http: HttpClient ) { }


  getPortfolios():Observable<Portfolio[]>{
  	let params = new HttpParams().append('_page','1');
        params = params.append('_limit','6');

    return this.http.get<Portfolio[]>(this.portfolioUrl, {params: params})

  }

}
