import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ScrollingModule, ScrollDispatcher } from '@angular/cdk/scrolling';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpService } from './http.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TopNavigationComponent } from './top-navigation/top-navigation.component';
import { FooterComponent } from './footer/footer.component';
import { SectionPortfolioComponent } from './section-portfolio/section-portfolio.component';
import { PortfolioGridComponent } from './portfolio-grid/portfolio-grid.component';
import { PortfolioItemComponent } from './portfolio-item/portfolio-item.component';
import { PortfolioModalComponent, PortfolioModalContent } from './portfolio-modal/portfolio-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TopNavigationComponent,
    FooterComponent,
    SectionPortfolioComponent,
    PortfolioGridComponent,
    PortfolioItemComponent,
    PortfolioModalComponent,
    PortfolioModalContent,  
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ScrollingModule,
   // ScrollDispatcher,
   // CdkScrollable
  ],
  entryComponents:[PortfolioModalContent],

  providers: [
     HttpService,
   //  CdkScrollable
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
