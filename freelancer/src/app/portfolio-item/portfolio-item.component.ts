import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { PortfolioModalComponent } from '../portfolio-modal/portfolio-modal.component';

@Component({
  selector: 'app-portfolio-item',
  templateUrl: './portfolio-item.component.html',
  styleUrls: ['./portfolio-item.component.scss']
})

export class PortfolioItemComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }
   
  openModal(){
  	 const modalRef = this.modalService.open(PortfolioModalComponent);
  } 
}
