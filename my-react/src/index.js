// import 'core-js/es/map';
// import 'core-js/es/set';

import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';

import avatar from './img/avataaars.svg';

import Navigation from'./components/Navigation';
import Masthead from'./components/Masthead';
import PageSectionPortfolio from'./components/PageSectionPortfolio';
import PageSectionAbout from'./components/PageSectionAbout';
import PageSectionContact from'./components/PageSectionContact';
import Footer from'./components/Footer';

import axios from 'axios';

//============= PageSection  ======================
class Page extends React.Component {

constructor(props) {  
    super(props);  
    this.state = {isScrollOn: false};

    // Эта привязка обязательна для работы `this` в колбэке.
    this.handleScroll = this.handleScroll.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
 
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
    let scrollStart = 100;
    let scroll = Math.round(window.scrollY);
    if(scroll > scrollStart) {
        this.setState(stateScroll => ({
        isScrollOn: stateScroll.isScrollOn = true
        }));
      }
    else{
    	this.setState(stateScroll => ({
        isScrollOn: stateScroll.isScrollOn = false
        }));
    }  
    
    // console.log('this.Scroll', scroll);
    // console.log('this.isScrollOn', this.state.isScrollOn);
  }

   
  handleClick () {
 //  const axios = require('axios'); 

   const usersUrl = 'http://localhost:3004/projects';
   var links;

   axios.get(usersUrl,
         { params: {
             _page: 2, 
             _limit: 6,} 
          })
        .then(function (response) {
          // handle success
          console.log(response);

          links = response.headers.link;
         
          var re = /\s*,\s*/;
          var splits = links.split(re);

          console.log('response.headers.link', links);
          console.log('splits =', splits);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed
        });


  } // End handleClick





  render() {
    return (
        <div id="page-top" onScroll={this.handleScroll}>
        
          <Navigation isStyleOn={this.state.isScrollOn} />
          <Masthead masthead={ section.masthead }/>
          <PageSectionPortfolio portfolios={ section.portfolios } />
          <PageSectionAbout about={section.about} />
          <PageSectionContact contact={section.contact} />

           {/* <div className="text-center mt-4">
             <button className="btn btn-xl btn-primary"
                onClick={ this.handleClick } >
                <i className="fas fa-download mr-2"></i>
                Http Client!
              </button>
            </div>*/}



          <Footer />
      
        </div>

     );
  } 
}

// ==========  Consts  =========
const section ={
		masthead:{
      id: 1,
      alias: 'masthead',
      addmenu: false,
			image: avatar,	
  		heading: 'Start Bootstrap',
  		subheading: 'Graphic Artist - Web Designer - Illustrator'
		},
		portfolios:{
      id: 2,
      alias: 'portfolios',
      addmenu: true,
      image: '',
			heading: 'Portfolio',
      subheading: ''
		},
		about:{
      id: 3,
      alias: 'about',
      addmenu: true,
      image: '',
			heading: 'About',
      subheading: ''
		},
		contact:{
      id: 4,
      alias: 'contact',
      addmenu: true,
      image: '',
			heading: 'Contact Me',
      subheading: ''
		}
	}
	

ReactDOM.render(
  <Page />,
  document.getElementById('root')
);

