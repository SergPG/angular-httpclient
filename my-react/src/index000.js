import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';


// function BoilingVerdict(props) {
//   if (props.celsius >= 100) {
//     return <p>The water would boil.</p>;
//   }
//   return <p>The water would not boil.</p>;
// }

// class Calculator extends React.Component {

//   constructor(props) {
//     super(props);
//     this.handleChange = this.handleChange.bind(this);
//     this.state = {temperature: ''};
//   }

//   handleChange(e) {
//     this.setState({temperature: e.target.value});
//   }


//   render() {
//     const temperature = this.state.temperature;
//     return (
//       <div className='contaner border border-primary p-3 mb-2 bg-secondary text-white' >
//       <fieldset>
//         <legend>Enter temperature in Celsius:</legend>
//         <input
//           value={temperature}
//           onChange={this.handleChange} />
//         <BoilingVerdict
//           celsius={parseFloat(temperature)} />
//       </fieldset>
//       </div>
//     );
//   }
// }

// ReactDOM.render(
//   <Calculator />,
//   document.getElementById('root')
// );

// function formatName(user) {
//   return user.firstName + ' ' + user.lastName;
// }


// function getGreeting(user) {
//   if (user) {
//     return <h1>Здравствуй, {formatName(user)}!</h1>;
//   }
//   return <h1>Здравствуй, незнакомец.</h1>;
// }

// const user = {
//   firstName: 'Марья',
//   lastName: 'Моревна'
// };

// const element = (
//   <div>
//     <h1>Здравствуйте!</h1>
//     <h2>Рады вас видеть.</h2>
//   </div>
// );

// ReactDOM.render(
//   element,
//   document.getElementById('root')
// );


/*   Clock    */
// class Clock extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {date: new Date()};
//   }

//   componentDidMount() {
//     this.timerID = setInterval(
//       () => this.tick(),
//       1000
//     );
//   }

//   componentWillUnmount() {
//     clearInterval(this.timerID);

//   }


//   tick() {
//     this.setState({
//       date: new Date()
//     });
//   }

//   render() {
//     return (
//       <div>
//         <h1>Привет, мир!</h1>
//         <h2>Сейчас {this.state.date.toLocaleTimeString()}.</h2>
//       </div>
//     );
//   }
// }
// //  ---- END Clock -----

// ReactDOM.render(
//   <Clock />,
//   document.getElementById('root')
// );

/* END  */




// function Welcome(props) {
//   return <h1>Привет, {props.name}</h1>;
// }

// const element = <Welcome name="Алиса" />;
// ReactDOM.render(
//   element,
//   document.getElementById('root')
// );

// function Welcome(props) {
//   return <h1>Привет, {props.name}</h1>;
// }

// function App() {
//   return (
//     <div className="p-3" >
//       <Welcome name="Алиса" />
//       <Welcome name="Базилио" />
//       <Welcome name="Буратино" />
//     </div>
//   );
// }

// ReactDOM.render(
//   <App />,
//   document.getElementById('root')
// );

// function formatDate(date) {
//   return date.toLocaleDateString();
// }

// //===================================
// function Avatar(props) {
//   return (
//     <img className="Avatar"
//       src={props.user.avatarUrl}
//       alt={props.user.name}
//     />

//   );
// }
// //=====================================
// function UserInfo(props) {
//   return (
//     <div className="UserInfo">
//       <Avatar user={props.user} />
//       <div className="UserInfo-name">
//         {props.user.name}
//       </div>
//     </div>
//   );
// }


// function Comment(props) {
//   return (
//     <div className="Comment p-3">
      
//       <UserInfo user={props.author} />

//       <div className="Comment-text">{props.text}</div>
//       <div className="Comment-date">
//         {formatDate(props.date)}
//       </div>
//     </div>
//   );
// }

// const comment = {
//   date: new Date(),
//   text: 'I hope you enjoy learning React!',
//   author: {
//     name: 'Hello Kitty',
//     avatarUrl: 'https://placekitten.com/g/64/64',
//   },
// };

// ReactDOM.render(
//   <Comment
//     date={comment.date}
//     text={comment.text}
//     author={comment.author}
//   />,
//   document.getElementById('root')
// );


class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggleOn: true,
      styleButton: "btn-success"

    };

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  }

  render() {
    return (
      <button className={this.state.isToggleOn ? 'btn btn-danger' : 'btn btn-success'} onClick={this.handleClick}>
         {this.state.isToggleOn ? 'Включено' : 'Выключено'}
      </button>
    );
  }
}

ReactDOM.render(
  <Toggle />,
  document.getElementById('root')
);
