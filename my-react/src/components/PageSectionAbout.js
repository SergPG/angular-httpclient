import React from 'react';

import IconDivider from'./IconDivider';

// ================ Const  ===========================

const contents = {
      texts: [
        {id:1,
         text: 'Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the complete source files including HTML, CSS, and JavaScript as well as optional SASS stylesheets for easy customization.',
         },
        {id:2, 
         text: 'You can create your own custom avatar for the masthead, change the icon in the dividers, and add your email address to the contact form to make it fully functional!',
         }
       ],
      linkButton: 'https://startbootstrap.com/themes/freelancer/'
    }



//  ==============  About Section Button  ===========
function AboutButton(props) {
  return (
    <div className="text-center mt-4">
       <a className="btn btn-xl btn-outline-light"
          href={ props.link } target="blank">
          <i className="fas fa-download mr-2"></i>
          Free Download!
        </a>
    </div>
    );
  }  

// ==============  About Texts ==============
function AboutTexts(props){

const texts = props.texts;

const textBlock = texts.map((block, index)=>
   <div className={ (index % 2 === 0) ? 'col-lg-4 ml-auto' : 'col-lg-4 mr-auto'}
    key={block.id} >
     <p className="lead"> { block.text }  </p>
   </div> 
);

  return (
    <div className="row">
         { textBlock } 
    </div>
  );
}

//============= Page Section About  ======================
class PageSectionAbout extends React.Component {

  constructor(props) {
       super(props);
       this.iconDividerLight = true;
  }

  render() {
    return (
      <section className="page-section bg-primary text-white mb-0" id="about">
        <div className="container">

          {/* -- Portfolio Section Heading --*/}
          <h2 className="page-section-heading text-center text-uppercase text-white">
              {this.props.about.heading}
          </h2>

         <IconDivider light={this.iconDividerLight} />

         <AboutTexts texts={contents.texts} />

         <AboutButton link={contents.linkButton} />

        </div>
      </section>      
     );
  } 
}

export default PageSectionAbout;