import React from 'react';

import IconDivider from'./IconDivider';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

import imgPortfolio1 from '../img/portfolio/cabin.png';
import imgPortfolio2 from '../img/portfolio/cake.png';
import imgPortfolio3 from '../img/portfolio/circus.png';
import imgPortfolio4 from '../img/portfolio/game.png';
import imgPortfolio5 from '../img/portfolio/safe.png';
import imgPortfolio6 from '../img/portfolio/submarine.png';
  

const portfoliosItms = [ 
  {id:1,
  title: 'Log Cabin',
  image: imgPortfolio1,
  text: '1 - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.'
   },
   {id:2,
  title: 'Tasty Cake',
  image: imgPortfolio2,
  text: '2 - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.'
   },
   {id:3,
  title: 'Circus Tent',
  image: imgPortfolio3,
  text: '3 - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.'
   },
   {id:4,
  title: 'Controller',
  image: imgPortfolio4,
  text: '4 - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.'
   },
   {id:5,
  title: 'Locked Safe',
  image: imgPortfolio5,
  text: '5 - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.'
   },
   {id:6,
  title: 'Submarine',
  image: imgPortfolio6,
  text: '6 - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.'
   },
  ];

//=========== Portfolio Card Modal  =========
function PortfolioVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="xl"
      dialogClassName="portfolio-modal"
      aria-labelledby={'portfolio-modal-'+ props.item.id }
      centered
    >
      <Modal.Header closeButton id={'portfolio-modal-'+ props.item.id}> </Modal.Header>

      <Modal.Body
         bsPrefix="modal-body text-center"
        >
         <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-8">
              {/*  <!-- Portfolio Modal - Title -->*/}
                <h2 className="portfolio-modal-title text-secondary text-uppercase mb-0">
                {props.item.title}</h2>

               {/* <!-- Icon Divider -->*/}
                <IconDivider light={false} />


               {/* <!-- Portfolio Modal - Image -->*/}
                <img className="img-fluid rounded mb-5" src={props.item.image} alt={props.item.title} />
              {/*  <!-- Portfolio Modal - Text -->*/}
                <p className="mb-5">
                  {props.item.text}
                </p>
                
                <Button className="btn btn-primary" onClick={props.onHide}>
                  <i className="fas fa-times fa-fw"></i>
                  Close Window
                </Button>

              </div>
            </div>
          </div>
      </Modal.Body>
    </Modal>
  );
}

function PortfolioItem(props) {
  const [modalShow, setModalShow] = React.useState(false);

  return (

      <div className="col-md-6 col-lg-4">
        <div className="portfolio-item mx-auto" onClick={(event) =>( 
          setModalShow(true))} >

            <div className="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div className="portfolio-item-caption-content text-center text-white">
                <i className="fas fa-plus fa-3x"></i>
              </div>
            </div>

            <img className="img-fluid" src={props.item.image} alt="" />
        </div>

      <PortfolioVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        item={props.item}
      />

      </div>
  );
}

// ==============  Portfolio Grid Items ==============
function PortfolioGridItems(props){

const portfolioItems = props.portfolioItems;

// console.log('portfolioItems', portfolioItems);

const portfolioItem = portfolioItems.map((item, index)=>
     <PortfolioItem item={item} key={item.id} /> 
);

  return ( portfolioItem );
}

//============================================================
//============= Page Section Portfolio  ======================
class PageSectionPortfolio extends React.Component {

  constructor(props) {
       super(props);
       this.iconDividerLight = false;
  }

  render() {
    return (
      <section className="page-section portfolio" id="portfolio">
        <div className="container">

          {/* -- Portfolio Section Heading --*/}
          <h2 className="page-section-heading text-center text-uppercase text-secondary mb-0">
              {this.props.portfolios.heading}
          </h2>

          <IconDivider light={this.iconDividerLight} />

          <div className="row">

                <PortfolioGridItems portfolioItems={portfoliosItms} />
                
          </div>      

        </div>
      </section>      

     );
  } 
}

export default PageSectionPortfolio;