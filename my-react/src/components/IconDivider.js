import React from 'react';
// import './IconDivider.scss';

//============= Icon Divider  ======================
const icon = "fas fa-star";

function IconDivider(props) {
  return (
    <div 
      className={ props.light ? 'divider-custom divider-light' : 'divider-custom' }
    >
        <div className="divider-custom-line"></div>
        <div className="divider-custom-icon">
          <i className={icon}></i>
        </div>
        <div className="divider-custom-line"></div>
    </div>
  );
}

export default IconDivider;