import React from 'react';

import IconDivider from'./IconDivider';

//============= Page Section Masthead  ======================
class Masthead extends React.Component {

  constructor(props) {
       super(props);
       this.iconDividerLight = true;
  }

  render() {
    return (   
     
        <header className="masthead bg-primary text-white text-center">
            <div className="container d-flex align-items-center flex-column">

            <img className="masthead-avatar mb-5"
                 src={this.props.masthead.image} 
                 alt="Avatar" />

            <h1 className="masthead-heading text-uppercase mb-0">
              {this.props.masthead.heading}</h1>

            <IconDivider light={this.iconDividerLight} />

            <p className="masthead-subheading font-weight-light mb-0">
               {this.props.masthead.subheading} </p>
        </div>
      </header>
   
     );
  } 
}

export default Masthead;