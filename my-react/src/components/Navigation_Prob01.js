import React from 'react';

import Navbar from 'react-bootstrap/Navbar';

import ScrollspyNav from'./ScrollspyNav';


//============= Page Section Navigation  ======================
class Navigation extends React.Component {

  render() {
    return (   

//======================================

<Navbar id="mainNav" bg="secondary" expand="lg" fixed="top" className={ this.props.isStyleOn ? 'text-uppercase navbar-shrink' : 'text-uppercase' }>
 <div className="container">

  <Navbar.Brand href="#page-top">Start React</Navbar.Brand>

  <Navbar.Toggle aria-controls="navbarResponsive" 
    className="navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" 
    children={<span>Menu <i className="fas fa-bars"></i></span>} 
    />

  <Navbar.Collapse id="navbarResponsive">

   <div className="navbar-nav ml-auto">
     <ScrollspyNav 
       scrollTargetIds={["portfolio", "about", "contact"]}
       activeNavClass="active"
       scrollDuration="1000"
     >
          <ul className="navbar-nav ml-auto">
           <li className="nav-item mx-0 mx-lg-1">
             <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Portfolio</a>
           </li>
           <li className="nav-item mx-0 mx-lg-1">
             <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">About</a>
           </li>
           <li className="nav-item mx-0 mx-lg-1">
             <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Contact</a>
           </li>
         </ul>

      </ScrollspyNav>
     </div>

    </Navbar.Collapse>

 </div> 
</Navbar>

//====================================
    );
  } 
}

export default Navigation;