import React from 'react';
import { Navbar, NavbarBrand, 
         NavbarToggler, Collapse } from 'reactstrap';

  import ScrollspyNav from'./ScrollspyNav';


export default class Navigation extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar id="mainNav" 
                color="secondary"
                expand="lg"
                fixed="top"
className={ this.props.isStyleOn ? 'text-uppercase navbar-shrink' : 'text-uppercase' }
                >

        <NavbarBrand href="#page-top">Start React</NavbarBrand>

        <NavbarToggler onClick={this.toggle} 
             className="navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" 
             children={<span>Menu <i className="fas fa-bars"></i></span>} 
        />

        <Collapse isOpen={this.state.isOpen} navbar> 

          <div className="ml-auto" navbar="true">

            <ScrollspyNav 
               scrollTargetIds={["portfolio", "about", "contact"]}
               activeNavClass="active"
               scrollDuration="1000"
            >
                <ul className="navbar-nav ml-auto">
                 <li className="nav-item mx-0 mx-lg-1">
                   <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Portfolio</a>
                 </li>
                 <li className="nav-item mx-0 mx-lg-1">
                   <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">About</a>
                 </li>
                 <li className="nav-item mx-0 mx-lg-1">
                   <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Contact</a>
                 </li>
               </ul>

            </ScrollspyNav>

           </div>

          </Collapse>
        </Navbar>
      </div>
    );
  }
}