import React from 'react';

//=============  Section Copyright  ======================
function Copyright(){
  return(
       <section className="copyright py-4 text-center text-white">
          <div className="container">
            <small>Copyright &copy; Your Website 2019</small>
          </div>
        </section> 
    );
}

//-------------------------------

const socialMenu = {
  title: 'Around the Web',
  items:[
      {id:1, icon:'fa-facebook-f', url:'https://www.facebook.com/'},
      {id:2, icon:'fa-twitter', url:'https://twitter.com/'},
      {id:3, icon:'fa-linkedin-in', url:'https://www.linkedin.com/'},
      {id:4, icon:'fa-dribbble', url:'https://dribbble.com/'},
  ],
}

//=============================

function SocialMenu(props) {

const socialMenu = props.socialMenu.items;

const socialMenuItem = socialMenu.map((item, index)=>
  <a key={item.id} 
     target="blank"
     className="btn btn-outline-light btn-social mx-1" 
     href={item.url}>
     <i className={'fab fa-fw '+ item.icon }></i>
  </a>
  );

return (
    <div className="socialMenus">
    <h4 className="text-uppercase mb-4"> {props.socialMenu.title} </h4> 
         { socialMenuItem } 
    </div>
  );

};



//============= Footer  ======================
class Footer extends React.Component {

  // constructor(props) {
  //      super(props);
       
  // }

  render() {
    return (
      <section>
          <footer className="footer text-center">
            <div className="container">
              <div className="row">
                
                {/*<!-- Footer Location -->*/}
                <div className="col-lg-4 mb-5 mb-lg-0">
                  <h4 className="text-uppercase mb-4">Location</h4>
                  <p className="lead mb-0">2215 John Daniel Drive </p>
                  <p className="lead mb-0">Clark, MO 65243</p>
                </div>

                 {/*<!-- Footer Social Icons -->*/}
                  <div className="col-lg-4 mb-5 mb-lg-0">

                    <SocialMenu socialMenu={ socialMenu } />

                  </div>

                 {/*<!-- Footer About Text -->*/}
                  <div className="col-lg-4">
                    <h4 className="text-uppercase mb-4">About Freelancer</h4>
                    <p className="lead mb-0">Freelance is a free to use, MIT licensed Bootstrap theme created by
                      <a target="blank" href="http://startbootstrap.com"> Start Bootstrap</a>
                     .</p>
                  </div>

              </div>
            </div>
          </footer>  

        <Copyright /> 
      </section>   
     );
  } 
}

export default Footer;